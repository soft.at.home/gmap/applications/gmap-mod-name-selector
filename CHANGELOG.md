# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.0.2 - 2024-04-10(10:07:21 +0000)

### Changes

- Make amxb timeouts configurable

## Release v1.0.1 - 2022-08-30(10:16:02 +0000)

### Other

- Add missing requires

## Release v1.0.0 - 2022-07-26(08:42:08 +0000)

### Other

- [prpl][gMap] gMap needs to be split in a gMap-core and gMap-client process

## Release v0.1.3 - 2022-05-24(08:05:37 +0000)

### Fixes

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

## Release v0.1.2 - 2021-09-14(09:11:51 +0000)

### Fixes

- [amxc] add ifdef around when_null_x, when_str_empty_x

## Release v0.1.1 - 2021-08-18(13:55:38 +0000)

### Fixes

- Change dependencies

### Other

- Disable packages CI job

## Release v0.1.0 - 2021-08-17(14:38:07 +0000)

### New

- Add implementation for name selection

